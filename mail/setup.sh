#!/bin/bash
bash /tmp/postfix/setup.sh
bash /tmp/dovecot/setup.sh

openssl req -new -x509 -nodes -out /etc/certs/mail.cert -keyout /etc/certs/mail.private -subj "/C=/ST=/L=/O=/OU=/CN=${MAIL_DOMAIN}"

postfix start
exec /sbin/my_init
