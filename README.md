The docker compose file runs 4 services

  1. postfix - MTA
  2. dovecot - IMAP server
  3. vimbadmin - UI for manage mailboxes
  4. roundcube - UI for use a mailbox

Open ports:  25 587 993 80

Usage

First run ./configure  and enter

  1. Email address domain
  2. Mail server domain
  3. The postmaster email address
  4. The vimdadmin domain
  5. Vimbadmin superuser
  6. Vimbadmin superuser password
  7. Roundcube domain

This will create a file `settings.env`

To run the server, use `docker-compose up --build -d` 

