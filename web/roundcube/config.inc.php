<?php

$config['db_dsnw'] = 'mysql://roundcube:password@mysql/roundcube';
$config['log_driver'] = 'syslog';
$config['default_host'] = 'imaps://mail';
$config['default_port'] = 993;
$config['smtp_server'] = 'tls://mail';
$config['smtp_port'] = 587;
$config['smtp_user'] = '%u';
$config['smtp_pass'] = '%p';
$config['imap_auth_type'] = "CHECK";
$config['plugins'] = array('archive', 'zipdownload', 'filesystem_attachments', 'hide_blockquote', 'password');
$config['language'] = 'en_US';
$config['preview_pane'] = true;
$config['mime_param_folding'] = 1;
$config['des_key'] = '%RANDOM_DES_KEY%';
$config['session_lifetime'] = 60;
